# `v2.0.11` (2022-06-01)
  - fixed: path error when parsing checkpoints stored in `./.checkpoints/`

# `v2.0.10` (2022-05-09)
  - changed: dependencies for installing packages by removing unnecessary dev packages which may cause conflicts in certain environments

# `v2.0.9` (2021-06-24)
  - fixed: bug when user.yaml was executed outside of current working dir by calling ioproc execute -u /path/to/user.yaml

# `v2.0.8` (2021-06-16)
  - Cached files from checkpoints are overwritten when already existing.
  - Improved error message when actionManager is missing, e.g. due to invalid actionPath.
  - Exceptions in actions are logged.
  - Logs are written with a timestamp.
  - Actions must be equipped with the required fields: project, call, data(read_from_dmgr, and write_to_dmgr. Except for checkpoints which are valided for project, call, and tag.
  - Development mode can be manually enabled to raise warnings but no exceptions.
  - Default folder projects was renamed to workflows with subfolder(s) workflow, each containing one user.yaml.
  - Improved testing
  - Executables for shell commands
  - Third action type when dmgr is not used
  - Comprehensive snippets to guide new users
  - Changed building to poetry package

# `v2.0.7` (2021-05-05)
  - Added: allowance of parameter global in the user.yaml which enables to define global parameters accessible for the whole workflow

# `v2.0.6` (2021-04-07)
  - datamanager supports dictionaries, lists, and strings in order to easily access datamanager items, both for reading and writing.
  - updated README.md and enabled syntax highlighting

# `v2.0.5` (2021-04-07)
  - changed directives in user.yaml to dictionary. lists are still supported but a deprecated feature.
  - datamanager supports dictionaries in order to easily access multiple datamanager items.
  - removed deprecated and unused requirements

# `v2.0.4` (2021-02-09)
  - Fixed bug: loading from cached files, when checkpoint feature is enabled, is possible again
  - Improved logging: when cache file with given tag is not available, an error message is logged and an Exception raised

# `v2.0.2` (2021-02-08)
  - Fixed bug: field args is now optional in user.yaml actions

# `v2.0.1` (2021-02-05)
  - Cached files from checkpoints are overwritten when already existing.
  - Improved error message when actionManager is missing, e.g. due to invalid actionPath.
  - Exceptions in actions are logged.
  - Logs are written with a timestamp.
  - Actions must be equipped with the required fields: project, call, data (with subfields read_from_dmgr, and  write_to_dmgr). Except for checkpoints which are valided for project, call, and tag.
  - Development mode can be manually enabled to raise warnings but no exceptions.
  - Default folder projects was renamed to workflows with subfolder(s) workflow, each containing one user.yaml.
